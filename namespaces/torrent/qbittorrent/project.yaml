---
# Source: qbittorrent/templates/ConfigurationConfigMap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: qbittorrent-config
data:
  # <USERNAME> and <PASSWORD> get replaced by the init container
  qBittorrent.conf: |
    [AutoRun]
    enabled=false
    program=

    [BitTorrent]
    Session\DefaultSavePath=/downloads/
    Session\Port=6881
    Session\QueueingSystemEnabled=true
    Session\TempPath=/downloads/incomplete/
    Session\DiskCacheSize=2000

    [LegalNotice]
    Accepted=true

    [Meta]
    MigrationVersion=3

    [Network]
    PortForwardingEnabled=false
    Proxy\AuthEnabled=true
    Proxy\HostnameLookupEnabled=false
    Proxy\IP=atlanta.us.socks.nordhold.net
    Proxy\Password=<NORD_PASSWORD>
    Proxy\Port=@Variant(\0\0\0\x85\x4\x38)
    Proxy\Profiles\BitTorrent=true
    Proxy\Profiles\Misc=true
    Proxy\Profiles\RSS=true
    Proxy\Type=SOCKS5
    Proxy\Username=<NORD_USERNAME>

    [Preferences]
    Connection\PortRangeMin=6881
    Connection\UPnP=false
    Downloads\SavePath=/downloads/
    Downloads\TempPath=/downloads/incomplete/
    WebUI\Address=*
    WebUI\Port=8443
    WebUI\ServerDomains=*
    WebUI\HTTPS\CertificatePath=/cluster-certs/tls.crt
    WebUI\HTTPS\Enabled=true
    WebUI\HTTPS\KeyPath=/cluster-certs/tls.key
    WebUI\Username=<USERNAME>
    WebUI\Password_PBKDF2="@ByteArray(<PASSWORD>)"
---
# Source: qbittorrent/templates/EnvConfigMap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: qbittorrent-env
data:
  PUID: "911"
  PGID: "911"
  TZ: "US/Eastern"
  WEBUI_PORT: "8443"
---
# Source: qbittorrent/templates/InitScriptConfigMap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: qbittorrent-init-script
data:
  password.py: |
    import os
    import hashlib
    import base64
    import sys

    ITERATIONS = 100_000
    SALT_SIZE = 16

    password = sys.argv[1]

    salt = os.urandom(SALT_SIZE)
    hash = hashlib.pbkdf2_hmac("sha512", password.encode(), salt, ITERATIONS)
    print(f"{base64.b64encode(salt).decode()}:{base64.b64encode(hash).decode()}")

  init.sh: |
    #!/bin/bash

    set -euo pipefail

    echo "Initializing"

    echo "Generating password hash for provided environment variable"
    password=$(python /script/password.py "$ADMIN_PASSWORD")
    echo "Hash: $password"

    echo "Copying config template to destination"
    cp /config/qBittorrent.conf /config-final/qBittorrent.conf

    echo "Replacing username placeholder with environment variable"
    sed -i "s/<USERNAME>/${ADMIN_USERNAME}/g" /config-final/qBittorrent.conf

    echo "Escaping password hash for sed"
    escaped_password=$(printf '%s\n' "$password" | sed -e 's/[\/&]/\\&/g')

    echo "Replacing password placeholder with escaped generated hash"
    sed -i "s/<PASSWORD>/${escaped_password}/g" /config-final/qBittorrent.conf

    echo "Initialization complete. Effective config is:"
    cat /config-final/qBittorrent.conf
---
# Source: qbittorrent/templates/Service.yaml
apiVersion: v1
kind: Service
metadata:
  name: qbittorrent
spec:
  type: ClusterIP
  selector:
    app: qbittorrent
  ports:
    - port: 443
      targetPort: 8443
      protocol: TCP
---
# Source: qbittorrent/templates/Deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: qbittorrent
spec:
  replicas: 1
  revisionHistoryLimit: 1
  selector:
    matchLabels:
      app: qbittorrent
  template:
    metadata:
      labels:
        app: qbittorrent
    spec:
      initContainers:
        - name: qbittorrent-setup
          imagePullPolicy: IfNotPresent
          image: python:3.12.4
          volumeMounts:
            - mountPath: /config
              name: config
            - mountPath: /config-final
              name: config-final
            - mountPath: /script
              name: script
          env:
            - name: ADMIN_USERNAME
              valueFrom:
                secretKeyRef:
                  key: username
                  name: qbittorrent-admin-account
            - name: ADMIN_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: password
                  name: qbittorrent-admin-account
          command:
            - bash
            - /script/init.sh
      containers:
        - name:  qbittorrent
          imagePullPolicy: IfNotPresent
          image: ghcr.io/linuxserver/qbittorrent:4.6.5
          envFrom:
            - configMapRef:
                name: qbittorrent-env
          resources:
            limits:
              memory: 3Gi
          ports:
            - containerPort: 8443
          volumeMounts:
            - mountPath: /downloads
              name: downloads
            - mountPath: /config/qBittorrent/qBittorrent.conf
              subPath: qBittorrent.conf
              name: config-final
            - mountPath: /cluster-certs
              name: cert
          startupProbe:
            periodSeconds: 10
            failureThreshold: 6
            tcpSocket:
              port: 8443
          readinessProbe:
            periodSeconds: 30
            failureThreshold: 3
            tcpSocket:
              port: 8443
          livenessProbe:
            periodSeconds: 30
            failureThreshold: 3
            tcpSocket:
              port: 8443
      volumes:
        - name: downloads
          hostPath:
            path: /home/craig/Downloads
        - name: config
          configMap:
            name: qbittorrent-config
        - name: cert
          secret:
            secretName: qbittorrent-internal-cert
        - name: config-final
          emptyDir: {}
        - name: script
          configMap:
            name: qbittorrent-init-script
---
# Source: qbittorrent/templates/Ingress.yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: qbittorrent
  annotations:
    cert-manager.io/cluster-issuer: craigmiller160-letsencrypt-cluster-issuer
    cert-manager.io/revision-history-limit: "1"
    nginx.ingress.kubernetes.io/proxy-body-size: 1000m
    nginx.ingress.kubernetes.io/proxy-buffer-size: 32k
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
spec:
  ingressClassName: public
  rules:
    - host: downloader.craigmiller160.us
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: qbittorrent
                port:
                  number: 443
  tls:
    - secretName: qbittorrent-tls
      hosts:
        - downloader.craigmiller160.us
---
# Source: qbittorrent/templates/Certificate.yaml
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: qbittorrent-internal-cert
spec:
  isCA: false
  secretName: qbittorrent-internal-cert
  revisionHistoryLimit: 1
  subject:
    organizations:
      - "Craig Miller"
    organizationalUnits:
      - Development
    countries:
      - US
  dnsNames:
    - craigmiller160.us
    - "qbittorrent"
    - "qbittorrent.torrent"
    - "qbittorrent.torrent.svc.cluster.local"
  privateKey:
    algorithm: ECDSA
    size: 256
  issuerRef:
    name: craigmiller160-selfsigned-cluster-issuer
    kind: ClusterIssuer
    group: cert-manager.io
---
# Source: qbittorrent/templates/OnepasswordItems.yaml
apiVersion: onepassword.com/v1
kind: OnePasswordItem
metadata:
  name: qbittorrent-admin-account
spec:
  itemPath: vaults/Home Server (Prod)/items/My Application Account
---
# Source: qbittorrent/templates/OnepasswordItems.yaml
apiVersion: onepassword.com/v1
kind: OnePasswordItem
metadata:
  name: qbittorrent-nord-account
spec:
  itemPath: vaults/Home Server (Prod)/items/NordVPN Service Account
