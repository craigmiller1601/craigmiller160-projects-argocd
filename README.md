# Craigmiller160 Projects ArgoCD

This repo contains the generated YAML files for all the projects deployed to the Craigmiller160 Kubernetes Cluster.

## Project Hierarchy

The hierarchy of projects is as follows:

namespaces/
    the-namespace-name/
        the-application-name/